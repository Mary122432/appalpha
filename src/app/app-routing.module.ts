import { NoIngresadoGuard } from './no-ingresado.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { IngresadoGuard } from './ingresado.guard';

const routes: Routes = [

  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule),
    canActivate:[NoIngresadoGuard]
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule),
    canActivate:[NoIngresadoGuard]
  },
   { 
    path: 'menu',
    loadChildren: () => import('./menu/menu.module').then( m => m.MenuPageModule),
    canActivate:[IngresadoGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'noticias',
    loadChildren: () => import('./pages/noticias/noticias.module').then( m => m.NoticiasPageModule)
  },
  {
    path: 'adquiere',
    loadChildren: () => import('./pages/adquiere/adquiere.module').then( m => m.AdquierePageModule)
  },
  {
    path: 'compras',
    loadChildren: () => import('./pages/compras/compras.module').then( m => m.ComprasPageModule)
  },
  {
    path: 'cardio-dance',
    loadChildren: () => import('./pages/entrenamientos/cardio-dance/cardio-dance.module').then( m => m.CardioDancePageModule)
  },
  {
    path: 'movimientos/:id',// /:id
    loadChildren: () => import('./movimientos/movimientos.module').then( m => m.MovimientosPageModule)
  },
  {
    path: 'notificaciones',
    loadChildren: () => import('./notificaciones/notificaciones.module').then( m => m.NotificacionesPageModule)
  },

  {
    path: 'citas', //:nombre
    loadChildren: () => import('./citas/citas.module').then( m => m.CitasPageModule)
  },

  {
    path: '**',  //alguna otra direccion inexistente redirige a home
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'citas',
    loadChildren: () => import('./citas/citas.module').then( m => m.CitasPageModule)
  },
  {
    path: 'datosusuario',
    loadChildren: () => import('./datosusuario/datosusuario.module').then( m => m.DatosusuarioPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
