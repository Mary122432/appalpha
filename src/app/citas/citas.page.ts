import { CitasService } from './../servicios/citas.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-citas',
  templateUrl: './citas.page.html',
  styleUrls: ['./citas.page.scss'],
})
export class CitasPage implements OnInit {

  citas:string[]=[];

  constructor(
    private CitasService:CitasService,
    private ActivatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    this.cargarCitas();
  } 

  cargarCitas(){
    //const id = this.ActivatedRoute.snapshot.params.id;
    this.CitasService.citas().subscribe(
      (data:any)=>{
        console.log(data) 
        this.citas=data;
      }, 
      err =>{
       console.log(err);
      }

    )

}

}
