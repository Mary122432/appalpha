import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NoticiasWebService {

  noticiasURL= 'http://d09852f23226.ngrok.io/clubalpha/api/v1/category/list/19/content';

  constructor(private http:HttpClient) { }
 
   noti(){
      const headers = new HttpHeaders({
        'Token': environment.Token
        });
      return this.http.get(environment.directionNoticias, { headers})
    }

}