import { Injectable } from '@angular/core';
//import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CitasService {

  citasURL = 'http://192.168.20.57:8090/citas/'; // URL SERVIDOR 

  constructor(private http:HttpClient) { }

  public citas(){  //nombre:string
    return this.http.get(this.citasURL + 'obtenerSalas'); //`obtenerSalas/${nombre}`
  }

}
