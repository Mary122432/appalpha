import { Injectable } from '@angular/core';
import { JwtDto } from '../models/jwt-dto';
import { Observable } from 'rxjs';
import { LoginUsuario } from 'src/app/models/login-usuario';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //authURL = 'http://localhost:8080/auth/'; //URL LOCAL
  authURL = 'http://192.168.20.57:8090/auth/'; // URL SERVIDOR 
  constructor(private httpClient: HttpClient ) { }
  
  public login(LoginUsuario:LoginUsuario):Observable<JwtDto>{
    return this.httpClient.post<JwtDto>(this.authURL + 'login', LoginUsuario);
  }

  public obtEstCuenta(){   //para obtener estado de cuenta
    return this.httpClient.get('');
  } 
}