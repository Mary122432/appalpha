import { ServiciosFoto } from './../models/servicios-foto';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}) 
export class FotoService {

  fotoURL = 'http://localhost:8080/alpha/'; //URL LOCAL
  //fotoURL = 'http://192.168.20.57:8090/alpha/'; // URL SERVIDOR 

  constructor(private httpClient:HttpClient) { }

   //Este metodo sirve para mostrar todas las fotos
  public obtFotos():Observable<ServiciosFoto[]>{  
    return this.httpClient.get<ServiciosFoto[]>(this.fotoURL + 'obtenerFoto')
  }

  public foto(FotoId:number):Observable<ServiciosFoto>{
    return this.httpClient.get<ServiciosFoto>(this.fotoURL + `obtenerFoto/${FotoId}`);   ///obtenerFoto/{fotoId} se concateno
  }  //se obtiene el id de la foto para que la pueda mostrar 
}


// Se debe obtener la foto por medio del idFoto no del token de inicio de sesion para eso cheacar la relacion de las tablas
//verificar el bloc de notas con las observaciones de la base de datos 