import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {ServiciosMovimientos} from  './../models/servicios-movimientos';

@Injectable({
  providedIn: 'root'
})
export class MovimientosService {
  //movURL = 'http://localhost:8080/alpha/'; //URL LOCAL
  movURL = 'http://192.168.20.57:8090/alpha/'; // URL SERVIDOR 

  constructor(private http:HttpClient) { }

   public movimientos(id:number):Observable<ServiciosMovimientos[]>{ 
    return this.http.get<ServiciosMovimientos[]>(this.movURL + `obtenerMovimientos/${id}`);
  }
}
