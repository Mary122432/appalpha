import { TokenService } from './token.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cliente } from './../models/cliente';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
}) 
export class ClienteService { 

    //clienteURL = 'http://localhost:8080/alpha/'; //URL LOCAL
    clienteURL = 'http://192.168.20.57:8090/alpha/'; // URL SERVIDOR 

  constructor(private httpClient:HttpClient, private TokenService:TokenService) { }

    //Este metodo sirve para mostrar un cliente, no necesita
    public obtCliente(id:number):Observable<Cliente[]>{
      return this.httpClient.get<Cliente[]>(this.clienteURL + `obtenerCliente/${id}`)//'obtenerCliente' + this.TokenService.getUserName) //+ this.TokenService.getUserName
                                                    //concatenar el id del cliente, aunque no lo pida 
    }
}
