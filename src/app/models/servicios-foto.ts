import {Binary} from "@angular/compiler"; //importacion agregada por el tipo de variable
import { Byte } from "@angular/compiler/src/util";

export class ServiciosFoto {
    id?:number; //opcional para mostrar id 
    Id_foto:number;  //atributos de la clase Foto.java
    imagen:Byte[]; // tipo de varibale byte[]
    Activo:boolean;
    FechaCreacion:Date;
    FechaModificacion:Date;

} 
