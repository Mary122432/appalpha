export class Cliente {
    id?:number;
    NoMembresia:number;
    nombre:string;
    apellidopaterno:string;
    apellidomaterno:string;
    nombrecompleto:string;
    Servicio:string;
    EstatusAcceso:string;
    TipoAcceso:boolean;
    DomicilioPago:boolean;
    InicioActividades:Date;
    Sexo:string;
    FechaNacimiento:Date;
    MensualidadPagada:boolean;
    Email:String; 
    FechaFinAcceso:Date;
    IdSexo:number;
    Nacionalidad:String;
    Telefono:String;
    IdClienteGrupo:number;
    IdClienteSector:number;
    IdCaptura:number;
    IdCapturaFecha:number;
    Activo:boolean;
    FechaCreación:Date;
    FechaModificacion:Date;
    tieneAcceso:boolean;


}
