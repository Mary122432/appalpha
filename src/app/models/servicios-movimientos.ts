export class ServiciosMovimientos {
    id?:number; //opcional
    IDClienteMovimiento:number;
    IDCliente:number;
    Cliente:string;
    Concepto:String;
    FechaDeAplicacion:Date;
    IDOrdenDeVenta:number;
    IDOrdenDeVentaDetalle:number;
    Debito:number;
    Saldo:number;
    Activo:boolean;
    FechaCreacion:Date;
    FechaModificacion:Date;
 

}
