// import { Component, OnInit } from '@angular/core';
// import {HttpClient} from '@angular/common/http';
// import {map} from 'rxjs/operators';

// @Component({
//   selector: 'app-perfil',
//   templateUrl: './perfil.page.html',
//   styleUrls: ['./perfil.page.scss'],
// })
// export class PerfilPage implements OnInit {

// info: any =[];

//   constructor(
//     private http: HttpClient) {

//      }

//   ngOnInit() {
//     this.getInfo().subscribe(res=>{console.log("Res", res)
//     this.info = res;
//   })

//   }

//   getInfo(){
//     return this.http
//     .get('assets/files/datos.json')
//     .pipe(
//       map((res:any)=>{
//         return res.data;
//       })
//     )
//   }


// }
import {Component, OnInit} from '@angular/core';
import {timer} from 'rxjs';
import { Profile1Service } from './profile1.service';
import { AuthService } from './../servicios/auth.service';
import {TokenService} from './../servicios/token.service'
import { LoginUsuario } from '../models/login-usuario';
import { FotoService } from './../servicios/foto.service';
import { ServiciosFoto as ServicesFoto, ServiciosFoto } from './../models/servicios-foto';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.page.html',
    styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit{

    fotos:ServiciosFoto[];  //guarda en un arreglo la lista de las fotos

    list: any;
    isLoading = true;
    isLogged=false;
    username=''; 
    perfilId: string;
  
    testLogged(): void {
        this.isLogged = this.tokenService.getToken() != null;
        this.username = this.tokenService.getUserName();
      }
    user = { 
        img:'', name: this.tokenService.getUserName()
    };
    
    constructor(private service: Profile1Service,public AuthService: AuthService, private tokenService: TokenService, private fotoService:FotoService, private act:ActivatedRoute
        , private http: HttpClient) {
      // this.list = this.service.getList();
    }

    ngOnInit() {

        //this.act.snapshot.paramMap.get('id');
        //this.http.get("http://localhost:8080/menu/perfil"+ this.perfilId).subscribe(res =>
        //{
        //console.log(res)
        //})
        timer(2000).subscribe(r => {
           this.isLoading = !this.isLoading;

        }
        );
       
        //this.cargarFoto(); //se manda a llamar el metodo de cargar imagen
    }

    ionViewWillEnter(){
        this.testLogged();
    }

    //Metodo para cargar todas las fotos
    cargarFoto():void{
        //const id= this.tokenService.getToken();
        this.fotoService.obtFotos().subscribe(  //esta linea esta bien solo hay que meter la variable en el metodo fotoy ya consultar
    data=>{
        this.fotos=data;
    },
    err => {
        console.log(err);
    }
        )
    } 
}
