import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DatosusuarioPage } from './datosusuario.page';

const routes: Routes = [
  {
    path: '',
    component: DatosusuarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DatosusuarioPageRoutingModule {}
