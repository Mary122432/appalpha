import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DatosusuarioPageRoutingModule } from './datosusuario-routing.module';

import { DatosusuarioPage } from './datosusuario.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosusuarioPageRoutingModule
  ],
  declarations: [DatosusuarioPage]
})
export class DatosusuarioPageModule {}
