import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardioDancePage } from './cardio-dance.page';

const routes: Routes = [
  {
    path: '',
    component: CardioDancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardioDancePageRoutingModule {}
