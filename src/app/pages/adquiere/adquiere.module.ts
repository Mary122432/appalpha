import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdquierePageRoutingModule } from './adquiere-routing.module';

import { AdquierePage } from './adquiere.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdquierePageRoutingModule,
    ComponentsModule
  ],
  declarations: [AdquierePage]
})
export class AdquierePageModule {}
