import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdquierePage } from './adquiere.page';

const routes: Routes = [
  {
    path: '',
    component: AdquierePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdquierePageRoutingModule {}
