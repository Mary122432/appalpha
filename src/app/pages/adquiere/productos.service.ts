import {Injectable} from '@angular/core';
import {ProductModel} from '../../components/custom-product-list/product.model';

@Injectable({providedIn: 'root'})
export class ProductosService {
    getList(): Array<ProductModel> {
      return [
        {name: 'Pase de AquaFitness', price: 99, img: 'assets/images/list/17.jpg'},
        {name: 'Pase de Pole Dance', price: 199, img: 'assets/images/list/18.jpg'},
        {name: 'Cycling Virtual', price: 299, img: 'assets/images/list/19.jpg'},
        {name: 'Evento especial', price: 399, img: 'assets/images/list/20.jpg'},
        {name: 'Body Combat', price: 99, img: 'assets/images/list/21.jpg'},
        {name: 'Body Fitness', price: 199, img: 'assets/images/list/22.jpg'},
    ];
    }
}
