import {Component, OnInit} from '@angular/core';
import {Home1Service} from './home1.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    list: Array<string>;
    list1: Array<any>;
    fList: Array<any>;
    tList: Array<any>;
    newList: Array<any>;

    option = {
      pagination: {
          el: '.swiper-pagination',
          type: 'progressbar',
      },
      navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
      },
      autoplay: {
          delay: 2000,
          disableOnInteraction: false,
      }
  };
    constructor(private service: Home1Service) {
        this.list = this.service.getBannerList();
        this.list1 = this.service.getList();
        this.fList = this.service.getFList();
        this.tList = this.service.getTList();
        this.newList = this.service.getNewList();
    }

    ngOnInit() {
    }

}
