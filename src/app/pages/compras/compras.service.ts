import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class ComprasService {
    getList(): Array<any> {
        return [
            {
                title: 'Pase Body Combat',
                date: ' Comprado en 15/07/2021',
                content: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac eros est. Cras iaculis pulvinar arcu non vehicula. Fusce at quam a eros malesuada condimentum. Aliquam tincidunt tincidunt vehicula.',
                img: 'assets/images/list/qr.png',
                usado:false
            },
            {
                title: 'Pase Rodillo de bici',
                date: ' Comprado en 12/07/2021',
                content: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac eros est. Cras iaculis pulvinar arcu non vehicula. Fusce at quam a eros malesuada condimentum. Aliquam tincidunt tincidunt vehicula.',
                img: 'assets/images/list/qr.png',
                usado:true
            },
            {
                title: 'Pase Aquafitness',
                date: ' Comprado en 10/07/2021',
                content: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac eros est. Cras iaculis pulvinar arcu non vehicula. Fusce at quam a eros malesuada condimentum. Aliquam tincidunt tincidunt vehicula.',
                img: 'assets/images/list/qr.png',
                usado:false
            },
            {
                title: 'Pase Pole Dance',
                date: ' Comprado en 08/07/2021',
                content: ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac eros est. Cras iaculis pulvinar arcu non vehicula. Fusce at quam a eros malesuada condimentum. Aliquam tincidunt tincidunt vehicula.',
                img: 'assets/images/list/qr.png',
                usado:false
            }
        ];
    }
}
