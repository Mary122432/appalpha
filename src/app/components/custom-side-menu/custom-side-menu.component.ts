import { NavController } from '@ionic/angular';
import {Component, Input, ViewEncapsulation} from '@angular/core';
import {SideMenuOption} from './models/side-menu-option';
import { AuthService } from './../../servicios/auth.service';
import { TokenService } from './../../servicios/token.service';

@Component({
    selector: 'custom-side-menu',
    templateUrl: './custom-side-menu.component.html',
    styleUrls: ['./custom-side-menu.component.scss'],
    encapsulation: ViewEncapsulation.None,
    
})
export class CustomSideMenuComponent {
    optionHeight = 45;
    paddingLeft = 16;
    @Input() menuList: Array<SideMenuOption>;
    @Input() user: any; 
    @Input() list: any;
 
    constructor( private navCtrl:NavController, private AuthService:AuthService, public TokenService:TokenService) {
    }

    toggle(item) {
        item.expanded = !item.expanded;
    }

    salir(){
      localStorage.setItem('ingresado','true');
      this.navCtrl.navigateRoot('/menu/login');
    }
}
