import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-custom-news',
  templateUrl: './custom-news.component.html',
  styleUrls: ['./custom-news.component.scss'],
})
export class CustomNewsComponent {
  @Input() list: Array<any>;

  constructor() { }
  toggleLiked(card: any) {

    if (card.icon === 'heart') {
      card.icon = 'heart-outline';
    } else {
      card.icon = 'heart';
    }
  }

  
}
