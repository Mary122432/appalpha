import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class ListToggleService {


    getList(): Array<any> {
        return [
            {
                title: '$400.00',
                subTitle: 'Cargo por mensualidad "Membresía vitual".',
                fecha:'25/07/2021',
                pago: false
            },
            {
                title: '$50.00',
                subTitle: 'Cargo por pase "Rodillo de bicicleta".',
                fecha:'20/07/2021',
                pago: false
            },
            {
                title: '$600.00',
                subTitle: 'Cargo por "Alphacuatic"',
                fecha:'10/07/2021',
                pago: false
            },
            {
                title: '$200',
                subTitle: 'Cargo por "Reimpresión de credencial"',
                fecha:'06/07/2021',
                pago: false
            },
            {
                title: '-$500.00',
                subTitle: 'Pago realizado por PayPal.',
                fecha:'05/07/2021',
                pago: true
            },
            {
                title: '$50.00',
                subTitle: 'Cargo por "Reimpresión de credencial"',
                fecha:'04/07/2021',
                pago: false
            },
            {
                title: '$70.00',
                subTitle: 'Cargo por "Reimpresión de credencial"',
                fecha:'02/07/2021',
                pago: false
            },
            {
                title: '$120.00',
                subTitle: 'Cargo por "Reimpresión de credencial"',
                fecha:'25/06/2021',
                pago: false
            },
        ];
    }
}
