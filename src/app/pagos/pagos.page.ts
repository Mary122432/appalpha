import { ListToggleService } from './list-toggle.service';
import { AlertProvider } from './../shared/providers/alert-provider';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-pagos',
  templateUrl: './pagos.page.html',
  styleUrls: ['./pagos.page.scss'],
})
export class PagosPage implements OnInit {

  list: Array<any>;

  constructor(private alertProvider: AlertProvider,
              private service: ListToggleService) {
      this.list = this.service.getList();
  }

  ngOnInit() {
  }

}
